## Add needed Node module
```bash
npm install get-image-colors

```

## Code example to loop thru images
```js
const path = require('path')
const fs = require('fs')
const getColors = require('get-image-colors')

fs.readdirSync('./').forEach(file => {
    getColors(path.join(__dirname, file)).then(colors => {
        console.log(file);
        console.log(colors.map(color => color.hex()))
    })
})
```
